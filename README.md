gl\_multi\_ci
=============

Allows a project to use multiple .gitlab-ci.yml files

Deployment
----------

- Generate ssh keys
- Add the private key as variable `MULTI_CI_PRIV_KEY` in *gl\_multi\_ci* repository
- Make the public key known - on wiki?

Adding projects
---------------

- Set up "destination" repository where CI builds will go
- Push empty master branch to it
- Add generated public key to *source* repository as deploy key
- Add same public key to *destination* repository with write access
- In "source" repository, set variable `MULTI_CI_TRIGGER_URL` to *gl\_multi\_ci* trigger URL
- Set variable `MULTI_CI_SRC_REPO` to git URL of the "source" repository
- Set variable `MULTI_CI_DEST_REPO` to "destination" repository URL
- In source `.gitlab-ci.yml`, add code from `./include-gitlab-ci.yml`

Viewing
-------

New builds will show up as branches in the CI repository


