#!/usr/bin/python3
import os
import subprocess
import re
from pathlib import Path
from collections import namedtuple
import shlex
from shutil import copyfile


CI_FILENAME = '.gitlab-ci.yml'
CI_FILE_REGEX = '\.gitlab\-ci\.(?P<name>[A-Za-z0-9-_]+)\.yml'
CONFIG_FILENAME = "multi_ci.vars"
CONFIG_TEMPLATE = """MULTI_CI_COMMIT_REF_NAME={}
MULTI_CI_COMMIT_SHA_SHORT={}
"""
START_FILENAME = 'start_multi_ci.sh'
START_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), START_FILENAME)


Committer = namedtuple('Committer', ['name', 'email', 'date'])

def make_config(ref, hash_):
    with open(CONFIG_FILENAME, "w") as f:
        print(CONFIG_TEMPLATE.format(shlex.quote(ref), shlex.quote(hash_)), file=f)

def run(params, *args, **kwargs):
    if 'check' not in kwargs:
        kwargs['check'] = True
    print(params)
    return subprocess.run(params, *args, **kwargs)

def checkout(hash_):
    run(['git', 'checkout', hash_], check=True)

def show(hash_, fmt):
    return run(['git', 'log', '-n1', '--pretty={}'.format(fmt), hash_],
               check=True, stdout=subprocess.PIPE).stdout \
                                                  .decode('utf-8') \
                                                  .strip()


def get_committer(hash_):
    name = show(hash_, '%cn')
    email = show(hash_, '%ce')
    date = show(hash_, '%ci')
    return Committer(name, email, date)

def get_message(hash_):
    return show(hash_, '%B')

def find_ci_files():
    for sub in Path('.').iterdir():
        if sub.is_file():
            match = re.search(CI_FILE_REGEX, sub.name)
            if match:
                yield match.groups('name')[0], sub.name


def create_branch(label):
    run(['git', 'branch', label], check=True)

def add(path):
    run(['git', 'add', path], check=True)

def commit(message, committer):
    env = dict(os.environ)
    env.update({'GIT_COMMITTER_NAME': committer.name,
                'GIT_COMMITTER_EMAIL': committer.email,
                'GIT_COMMITTER_DATE': committer.date})
    run(['git', 'commit',
         '--author={}'.format("CI builder service <czaplejewicz@mpipz.mpg.de>"),
         '-m', message],
        env=env,
        check=True)

def create_tag(label):
    run(['git', 'tag', label], check=True)

def push_ref(ref, remote):
    run(['git', 'push', remote, 'origin/{}'.format(ref, ref)], check=True)

def push_tag(label, remote):
    run(['git', 'push', remote, 'tags/{}'.format(label)], check=True)

def push_branch(label, remote):
    run(['git', 'push', remote, '{}:{}'.format(label, label)], check=True)


def get_short_hash(hash_):
    return run(['git', 'describe', '--always', hash_], check=True, stdout=subprocess.PIPE).stdout \
              .decode('utf-8') \
              .strip()

def rename_and_push(path, remote, ref, hash_):
    os.chdir(path)
    committer = get_committer(hash_)
    short_hash = get_short_hash(hash_)
    checkout(hash_)
    for ciname, cifile in find_ci_files():
        print("Processing {}".format(cifile))
        checkout(hash_)
        label = '{}-{}-{}'.format(ref, short_hash, ciname)
        create_branch(label)
        checkout(label)
        os.rename(CI_FILENAME, CI_FILENAME + '.bak')
        os.rename(cifile, CI_FILENAME)
        add(cifile)
        add(CI_FILENAME + '.bak')
        add(CI_FILENAME)
        make_config(ref, hash_)
        add(CONFIG_FILENAME)
        copyfile(START_PATH, START_FILENAME)
        add(START_FILENAME)
        commit('CI {}: {}'.format(ciname, get_message(hash_)), committer=committer)
        print("Pushing")
        push_branch(label, remote=remote)
    

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("directory", help='Path to git repository')
    parser.add_argument("remote", help='CI remote name')
    parser.add_argument("ref", help='Name of reference being tested')
    parser.add_argument('hash', help='Commit being tested')
    args = parser.parse_args()
    
    rename_and_push(args.directory, args.remote, args.ref, args.hash)
