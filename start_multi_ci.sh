#! /bin/sh

# Checks out the same branch and commit as the original that had to be replaced for multi-ci

set -e

. ./multi_ci.vars

if ! git checkout "$MULTI_CI_COMMIT_REF_NAME"; then
  git checkout -b "$MULTI_CI_COMMIT_REF_NAME"
fi;
git reset --hard "$MULTI_CI_COMMIT_SHA_SHORT"
  
